# Запуск
Для запуска передать имя входного файла в качестве аргумента
Результат в виде WAT или ошибки будут выведены в консоль

# EBNF
~~~
 <program> = <statement>
 <statement> = ("if (<sexpr>) { <statement> } " |
                "if (<sexpr>) { <statement> } else { <statement> }" |
                "while (<sexpr>) { <statement> } " |
                "let <var> = <sexpr>; "), <statement>
 <sexpr> = <expr> == <expr> |
            <expr> != <expr> |
            <expr> > <expr> |
            <expr> < <expr> |
            <expr>
 <expr> = <term> { +|- <expr> }
 <term> = <fact> { *|/ <term> }
 <fact> = <num> | <var> | (<sexpr>)
~~~
     

