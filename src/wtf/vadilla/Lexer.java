package wtf.vadilla;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {

    private Matcher matcher;
    private Type nowType;
    private String nowValue;

    // доступные лексемы вместе со своей регуляркой
    // порядок перечисления важен(EQ идет до ASS, VAR после всего)
    public enum Type {
        IF("if"),
        ELSE("else"),
        WHILE("while"),
        LET("let"),
        SEMI(";"),
        EQ("=="),
        ASS("="),
        LESS("<"),
        MORE(">"),
        NEQ("!="),
        ADD("\\+"),
        SUB("-"),
        MUL("\\*"),
        DIV("/"),
        LBR("\\("),
        RBR("\\)"),
        LCUR("\\{"),
        RCUR("\\}"),
        VAL("\\d+"),
        VAR("[a-zA-Z]+"),
        EOF("\\z");

        String regex;

        Type(String regex) {
            this.regex = regex;
        }
    }

    public Lexer(String data) {
        StringBuilder sb = new StringBuilder();

        // собираем регулярку в которой будут все виды лексем
        for (Type type : Type.values()) {
            sb.append(type.regex);
            sb.append("|");
        }
        sb.deleteCharAt(sb.length() - 1);

        // используем матчер, чтобы идти по лексемам
        this.matcher = Pattern.compile(sb.toString()).matcher(data);
    }

    public Type getNowType() {
        return this.nowType;
    }

    public String getNowData() {
        return this.nowValue;
    }

    public int getNowOffset() {
        return matcher.start();
    }

    public void nextToken() {
        if (matcher.find()) {
            // записываем найденную лексему в исходном виде
            this.nowValue = matcher.group();
            // сопоставляем тип с помощью перебора
            for (Type type : Type.values()) {
                if (this.nowValue.matches(type.regex)) {
                    this.nowType = type;
                    break;
                }
            }
        }
    }

}
