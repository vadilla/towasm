package wtf.vadilla;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CLI {

    // собирает строку в которой будет показано место ошибки
    private static String decorateError(String sourceCode, int pos) {
        // обрабатываем случай, когда парсер дошел до конца файла
        if (pos >= sourceCode.length()) {
            pos = sourceCode.length() - 1;
        }
        // пропускаем пробелы и переносы строк
        while (pos > 0 && Character.isWhitespace(sourceCode.charAt(pos - 1))) {
            pos--;
        }

        // находим позиции границ строки
        int r = (sourceCode.indexOf('\n', pos));
        if (r == -1) {
            r = sourceCode.length();
        }
        int l = sourceCode.lastIndexOf('\n', r - 1);
        if (l == -1) {
            l = 0;
        }

        // добавляем пробелы и крышечку
        StringBuilder result = new StringBuilder();
        result.append(sourceCode, l, r);
        result.append('\n');
        for (int i = l; i < pos; i++) {
            result.append(" ");
        }
        result.append("^");
        return result.toString();
    }

    // имя файла в 1 аргументе, читаем из него, выводим в stdout
    public static void work(String[] args) {
        if (args.length == 0) {
            System.out.println("specify input file");
        }

        // считываем весь файл в строку
        String sourceCode;
        try {
            sourceCode = new String(Files.readAllBytes(Paths.get(args[0])));
        } catch (IOException ex) {
            System.out.println("Failed to open the file");
            System.out.println(ex.toString());
            return;
        }

        // запускаем парсер
        Node tree;
        try {
            Parser parser = new Parser(sourceCode);
            tree = parser.parse();
        } catch (ParseException ex) {
            System.out.println("Syntax error: ");
            System.out.println(ex.toString());
            System.out.println(decorateError(sourceCode, ex.getPos()));
            return;
        }
        // компилируем и выводим
        Compiler compiler = new Compiler();
        String wat = compiler.compile(tree);
        System.out.println(wat);
    }
}
