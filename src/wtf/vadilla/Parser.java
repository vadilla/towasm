package wtf.vadilla;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import wtf.vadilla.Lexer.Type;


class Node {
    private Lexer.Type type;
    private Map<String, Node> children;
    private String value;

    public Node(Lexer.Type type) {
        this.type = type;
        children = new HashMap<>();
    }

    public Type getType() {
        return type;
    }

    public Set<String> getChildren() {
        return children.keySet();
    }

    public Node getChild(String tag) {
        return children.get(tag);
    }

    public String getValue() {
        return value;
    }

    public void setChild(String tag, Node child) {
        children.put(tag, child);
    }

    public void setValue(String value) {
        this.value = value;
    }
}


class ParseException extends Exception {
    private int pos;

    public ParseException(String msg, int pos) {
        super(msg);
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }
}

// из нескольких таких классов собирается шаблон для парсинга
// хранит информацию о том, как проверить лексему и какие действие совершить
class Construct {

    // MATCH - если лексемы не совпадают, то бросиить исключение
    // CHECK - нестрогая проверка совпадений
    // SEX - выражение уровнем ниже
    // STAT - утверждение уровнем ниже
    enum Action {
        MATCH,
        CHECK,
        SEX,
        STAT;
    }

    private Action action;
    private boolean getData;
    private String child_name;
    private Type type;

    public Construct(Action action, Type type, boolean getData) {
        this.action = action;
        this.type = type;
        this.getData = getData;
    }

    public Construct(Action action, String child_name) {
        this.action = action;
        this.child_name = child_name;
    }

    public Action getAction() {
        return action;
    }

    public boolean isGetData() {
        return getData;
    }

    public String getChildName() {
        return child_name;
    }

    public Type getType() {
        return type;
    }
}

// собирает и хранит в себе последовательность для проверки лексем
class Construction {
    private Type before, after;
    private Construct[] constructs;

    public Construction(Type type, String template) {
        this(type, type, template);
    }

    // шаблон собирается из строки, токены разделяются запятой и имеют несколько видов
    // ABC - строковое представление enum Type для проверки на совпадение
    // перед токеном можно добавить модификаторы
    // ! - делает токен нестрогим для проверки
    // % - из токена нужно достать данные
    // <child:type> - две строки, разделенные двоеточием
    // такой токен говорит о том, что нужно создать сына с именем "child" и типом "type"
    public Construction(Type before, Type after, String template) {
        this.before = before;
        this.after = after;

        ArrayList<Construct> tmpConstructs = new ArrayList<>();

        for (String pattern : template.split(",")) {
            if (pattern.matches("<[a-z]+:[a-z]+>")) {
                String childName = pattern.substring(1, pattern.indexOf(':'));
                String typeName = pattern.substring(pattern.indexOf(':') + 1, pattern.length() - 1);

                if (typeName.equals("sex")) {
                    tmpConstructs.add(new Construct(Construct.Action.SEX, childName));
                } else if (typeName.equals("stat")) {
                    tmpConstructs.add(new Construct(Construct.Action.STAT, childName));
                }
            } else {
                boolean getData = false;

                Construct.Action action = Construct.Action.MATCH;
                if (pattern.startsWith("!")) {
                    pattern = pattern.substring(1);
                    action = Construct.Action.CHECK;
                }
                if (pattern.startsWith("%")) {
                    pattern = pattern.substring(1);
                    getData = true;
                }
                Type expected = Type.valueOf(pattern);
                tmpConstructs.add(new Construct(action, expected, getData));
            }
        }
        constructs = new Construct[tmpConstructs.size()];
        constructs = tmpConstructs.toArray(constructs);
    }

    public Type getBefore() {
        return before;
    }

    public Type getAfter() {
        return after;
    }

    public Construct[] getConstructs() {
        return constructs;
    }
}

// будем рекурсивно парсить, генерируя дерево синтаксиса
// чтобы понять, в каком порядке запускать функции можно описать язык с помощью EBNF

public class Parser {

    private Lexer lexer;
    private Map<Type, Construction> statementTemplates;


    public Parser(String data) {
        lexer = new Lexer(data);
    }

    public Node parse() throws ParseException {
        lexer.nextToken();
        statementTemplates = new HashMap<>();

        statementTemplates.put(
                Type.IF,
                new Construction(Type.IF, "IF,LBR,<cond:sex>,RBR,LCUR,<body:stat>,RCUR,!ELSE,LCUR,<else:stat>,RCUR")
        );
        statementTemplates.put(
                Type.WHILE,
                new Construction(Type.WHILE, "WHILE,LBR,<cond:sex>,RBR,LCUR,<body:stat>,RCUR")
        );
        statementTemplates.put(
                Type.VAR,
                new Construction(Type.VAR, Type.ASS, "%VAR,ASS,<body:sex>,SEMI")
        );
        statementTemplates.put(
                Type.LET,
                new Construction(Type.LET, "LET,%VAR,ASS,<body:sex>,SEMI")
        );

        // программа является утверждением => вызываем соотв. функцию
        return statement();
    }

    // чтобы не копировать код, будем парсить утверждения по шаблону
    private Node parseStatement(Type type) throws ParseException {
        Construction construction = statementTemplates.get(type);
        Node res = new Node(construction.getAfter());

        for (Construct construct : construction.getConstructs()) {
            if (construct.isGetData()) {
                res.setValue(lexer.getNowData());
            }
            switch (construct.getAction()) {
                case MATCH:
                    if (lexer.getNowType() != construct.getType()) {
                        throw new ParseException("expected " + construct.getType(), lexer.getNowOffset());
                    }
                    break;
                case CHECK:
                    if (lexer.getNowType() != construct.getType()) {
                        return res;
                    }
                    break;
                case SEX:
                    res.setChild(construct.getChildName(), sexpression());
                    continue;
                case STAT:
                    res.setChild(construct.getChildName(), statement());
                    continue;
            }
            lexer.nextToken();
        }
        return res;
    }


    private Node statement() throws ParseException {
        Node res;
        if (statementTemplates.containsKey(lexer.getNowType())) {
                res = parseStatement(lexer.getNowType());
        } else {
            throw new ParseException("expected statement", lexer.getNowOffset());
        }

        // т.к. увтверждение может состоять из нескольких операторов, то
        // вводим доп. атрибут next для последовательного перечисления утверждений
        if (lexer.getNowType() == Type.IF
                || lexer.getNowType() == Type.WHILE
                || lexer.getNowType() == Type.LET
                || lexer.getNowType() == Type.VAR) {
            res.setChild("next", statement());
        }
        return res;
    }

    private Node sexpression() throws ParseException {
        Node left = expression();
        Type nowType = lexer.getNowType();

        // проверяем только один раз, т.к. несколько операторов сравнения должны быть разделены скобками
        if (nowType == Type.EQ || nowType == Type.NEQ || nowType == Type.MORE || nowType == Type.LESS) {
            Node res = new Node(nowType);
            lexer.nextToken();
            res.setChild("left", left);
            res.setChild("right", expression());
            return res;
        }
        return left;
    }

    private Node expression() throws ParseException {
        Node now = term();

        // строим дерево в "перевернутом" порядке, чтобы вычитание шло последовательно
        while (lexer.getNowType() == Type.ADD || lexer.getNowType() == Type.SUB) {
            Node next = new Node(lexer.getNowType());
            lexer.nextToken();
            next.setChild("left", now);
            next.setChild("right", term());
            now = next;
        }
        return now;
    }

    private Node term() throws ParseException {
        Node now = factor();

        // строим дерево в "перевернутом" порядке, чтобы деление шло последовательно
        while (lexer.getNowType() == Type.MUL || lexer.getNowType() == Type.DIV) {
            Node next = new Node(lexer.getNowType());
            lexer.nextToken();
            next.setChild("left", now);
            next.setChild("right", factor());
            now = next;
        }
        return now;

    }

    private Node factor() throws ParseException {
        Type nowType = lexer.getNowType();

        // если текущий токен - число или переменная,
        // то вытакскиваем данные из лексемы и создаем лист в дереве
        if (nowType == Type.VAL || nowType == Type.VAR) {
            Node res = new Node(lexer.getNowType());
            res.setValue(lexer.getNowData());
            lexer.nextToken();
            return res;
        }
        // иначе продолжаем парсинг
        if (nowType == Type.LBR) {
            lexer.nextToken();
            Node res = sexpression();
            // проверяем, что выражение закрыто скобкой
            if (lexer.getNowType() != Type.RBR) {
                throw new ParseException("expected " + Type.RBR, lexer.getNowOffset());
            }
            lexer.nextToken();
            return res;
        }
        // кидаем исключение, т.к. ни один элемент не может выражать множитель
        throw new ParseException("expected expression", lexer.getNowOffset());
    }
}
