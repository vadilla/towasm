package wtf.vadilla;

public class Util {

    private static void space(int n) {

        for (int i = 0; i < n - 1; i++) {
            System.out.print(" ");
        }
        if (n > 0) {
            System.out.print("|");
        }
    }

    private static void dfs(Node v, int depth) {
        space(depth);
        if (v == null) {
            System.out.println("null");
            return;
        }

        System.out.print(v.getType());
        if (v.getValue() != null) {
            System.out.print(" " + v.getValue());
        }
        System.out.println();

        for (String child: v.getChildren()) {
            space(depth);
            System.out.println(child);
            dfs(v.getChild(child), depth + 1);
        }

    }

    public static void printTree(Node tree) {
        dfs(tree, 0);
    }


    public static void printLexems(Lexer lexer) {
        lexer.nextToken();
        while (lexer.getNowType() != Lexer.Type.EOF) {
            System.out.println(lexer.getNowType() + " " + lexer.getNowData());
            lexer.nextToken();
        }
    }
}
