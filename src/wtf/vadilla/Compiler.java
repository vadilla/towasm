package wtf.vadilla;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import wtf.vadilla.Lexer.Type;


public class Compiler {
    private ArrayList<String> localVariables;
    private StringBuilder resultBody;
    private int depth;

    private Map<Type, String> pattern;
    private String watTemplate = "(module\n (func (export \"fu\") %s\n%s )\n)";

    public Compiler() {
        localVariables = new ArrayList<>();
        resultBody = new StringBuilder();
        depth = 0;

        pattern = new HashMap<>();
        pattern.put(Type.ADD, "(i32.add)");
        pattern.put(Type.SUB, "(i32.sub)");
        pattern.put(Type.MUL, "(i32.mul)");
        pattern.put(Type.DIV, "(i32.div_s)");
        pattern.put(Type.EQ, "(i32.eq)");
        pattern.put(Type.NEQ, "(i32.ne)");
        pattern.put(Type.MORE, "(i32.gt_s)");
        pattern.put(Type.LESS, "(i32.lt_s)");
        pattern.put(Type.VAL, "(i32.const [value])");
        pattern.put(Type.VAR, "(get_local $[value])");
    }

    // строки разделены через запятую
    // +/- - спецсимволы для изменения глубины
    private void writeLine(String line) {
        for (String word : line.split(",")) {
            if (word.equals("+")) {
                depth++;
                continue;
            }
            if (word.equals("-")) {
                depth--;
                continue;
            }

            for (int i = 0; i < depth; i++) {
                resultBody.append(" ");
            }
            resultBody.append(word);
            resultBody.append("\n");
        }
    }

    private void constructAss(Node v) {
        if (v.getType() == Type.LET) {
            localVariables.add(v.getValue());
        }
        dfs(v.getChild("body"));
        writeLine("(set_local $" + v.getValue() + ")");
    }

    private void constructIf(Node v) {
        writeLine("(if,+,(block (result i32),+");
        dfs(v.getChild("cond"));
        writeLine("-,),(then,+");
        dfs(v.getChild("body"));
        writeLine("-,)");

        if (v.getChildren().contains("else")) {
            writeLine("(else,+");
            dfs(v.getChild("else"));
            writeLine("-,)");
        }
        writeLine("-,)");
    }

    private void constructWhile(Node v) {
        writeLine("(if,+,(block (result i32),+");
        dfs(v.getChild("cond"));
        writeLine("-,),(then,+,(loop,+");
        dfs(v.getChild("body"));
        writeLine("(br_if 0,+,(block (result i32),+");
        dfs(v.getChild("cond"));
        writeLine("-,),-,),-,),-,),-,)");
    }

    private void dfs(Node v) {
        switch (v.getType()) {
            case IF:
                constructIf(v);
                break;
            case ASS:
            case LET:
                constructAss(v);
                break;
            case WHILE:
                constructWhile(v);
                break;
            case VAR:
            case VAL:
                writeLine(pattern.get(v.getType()).replace("[value]", v.getValue()));
                break;
            default:
                dfs(v.getChild("left"));
                dfs(v.getChild("right"));
                writeLine(pattern.get(v.getType()));
        }

        if (v.getChildren().contains("next")) {
            dfs(v.getChild("next"));
        }
    }

    // обходим дерево с помощью dfs поддерживая глубину
    public String compile(Node tree) {
        localVariables.clear();
        resultBody.setLength(0);
        depth = 2;

        dfs(tree);

        StringBuilder locals = new StringBuilder();

        for (String var : localVariables) {
            locals.append(String.format("(local $%s i32) ", var));
        }

        return String.format(watTemplate, locals, resultBody);
    }
}
